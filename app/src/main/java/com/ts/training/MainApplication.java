package com.ts.training;

import android.app.Application;
import android.util.Log;
import android.widget.Toast;

import com.ts.mobile.sdk.AuthenticationError;
import com.ts.mobile.sdk.CollectorType;
import com.ts.mobile.sdk.LogLevel;
import com.ts.mobile.sdk.SDKConnectionSettings;
import com.ts.mobile.sdk.TransmitSDK;
import com.ts.mobile.sdk.util.ObservableFuture;
import com.ts.mobile.sdk.util.defaults.DefaultTransportProvider;
import com.ts.training.authenticators.uiHandler.UIHandlerManager;

import java.net.CookieHandler;
import java.net.CookieManager;
import java.util.ArrayList;

public class MainApplication extends Application {

    public static final String TAG = "TransmitApp";
    @Override
    public void onCreate() {
        super.onCreate();
        TransmitSDK.setApplicationContext(this);
        SDKConnectionSettings sdkConnectionSettings = SDKConnectionSettings.create(
                getString(R.string.server_base_address),
                getString(R.string.app_id_from_console),
                getString(R.string.token_name),
                getString(R.string.token_value));

        //Here we can set the REALM
        sdkConnectionSettings.setRealm(getString(R.string.realm));

        TransmitSDK.getInstance().setConnectionSettings(sdkConnectionSettings);
        initializeTransmitLog();
        collectDeviceAttributes();

        //Add Firebase messageing for PUSH NOTIFICATION
        //initFirebaseToken();

        CookieHandler.setDefault(new CookieManager());

        DefaultTransportProvider.Builder defaultTransportProvider = new DefaultTransportProvider.Builder();
        TransmitSDK.getInstance().setTransportProvider(defaultTransportProvider.build());

        TransmitSDK.getInstance().setUiHandler(new UIHandlerManager());

        initializeTransmitSdk();
    }
    private void initializeTransmitLog() {
        TransmitSDK.getInstance().setLogLevel(LogLevel.Debug);
//        TransmitSDK.getInstance().setExternalLogger((level, category, message) -> {
//            Log.e(TAG, "initializeTransmitLog log: Category is : " + category + " message is : " + message);
//        });
    }
    private void collectDeviceAttributes() {
        TransmitSDK.getInstance().setEnabledCollectors(new ArrayList<CollectorType>(10) {{
            add(CollectorType.Accounts);
            add(CollectorType.Software);
            add(CollectorType.Bluetooth);
            add(CollectorType.Capabilities);
            add(CollectorType.DeviceDetails);
            add(CollectorType.ExternalSDKDetails);
            add(CollectorType.LargeData);
            add(CollectorType.Location);
            add(CollectorType.Owner);
            add(CollectorType.HWAuthenticators);
        }});
    }
    private void initializeTransmitSdk() {
        TransmitSDK.getInstance().initialize().addListener(new ObservableFuture.Listener<Boolean, AuthenticationError>() {
            public void onComplete(Boolean aBoolean) {
                Toast.makeText(getApplicationContext(), "TransmitSDK initialize compelte", Toast.LENGTH_SHORT).show();
            }
            @Override
            public void onReject(AuthenticationError authenticationError) {
                Log.e(TAG, authenticationError.getErrorCode() + "\n" + authenticationError.getMessage());
            }
        });
    }
//    private void initFirebaseToken() {
//        String pushToken = FirebaseInstanceId.getInstance().getToken();
//        TransmitSDK.getInstance().setPushToken(pushToken);
//    }
//
}
