package com.ts.training.authenticators.information;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.ts.training.R;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link InformationFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class InformationFragment extends Fragment {

    public interface InformationFragmentInterface {
        void onPromiseFutureComplete();
    }

    private InformationFragmentInterface listener;
    private Button continueBtn;

    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_TITLE = "title";
    private static final String ARG_TEXT = "text";
    private static final String ARG_CONTINUE_TEXT = "continueText";

    private String mTitle;
    private String mText;
    private String mContinueText;

    public InformationFragment() {
        // Required empty public constructor
    }

    public InformationFragment(InformationFragmentInterface context) {
        if(context instanceof InformationFragmentInterface){
            this.listener = context;
        } else {
            throw new RuntimeException("listener is not an instance of InformationFragmentInterface");
        }
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment InformationFragment.
     */
    public static InformationFragment newInstance(InformationFragmentInterface context, String param1, String param2, String param3) {
        InformationFragment fragment = new InformationFragment(context);
        Bundle args = new Bundle();
        args.putString(ARG_TITLE, param1);
        args.putString(ARG_TEXT, param2);
        args.putString(ARG_CONTINUE_TEXT, param3);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mTitle = getArguments().getString(ARG_TITLE);
            mText = getArguments().getString(ARG_TEXT);
            mContinueText = getArguments().getString(ARG_CONTINUE_TEXT);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.information_fragment, container, false);
        TextView informationTitle = view.findViewById(R.id.information_title);
        TextView informationText = view.findViewById(R.id.information_text);
        continueBtn = view.findViewById(R.id.information_btn_ok);

        informationTitle.setText(mTitle);
        informationText.setText(mText);
        continueBtn.setText(mContinueText);

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        continueBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onPromiseFutureComplete();

                requireActivity().getSupportFragmentManager().beginTransaction()
                        .remove(InformationFragment.this)
                        .commit();
            }
        });

    }
}