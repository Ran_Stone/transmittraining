package com.ts.training.authenticators.information;

import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.ts.mobile.sdk.ConfirmationInput;
import com.ts.mobile.sdk.PolicyAction;
import com.ts.mobile.sdk.util.PromiseFuture;
import com.ts.training.authenticators.uiHandler.UIHandlerClientContext;

import java.util.Map;

/**
 * Created by Ran Stone on 01/11/2020.
 */
public class InformationSession extends PromiseFuture<ConfirmationInput, Void>
        implements InformationFragment.InformationFragmentInterface {

    final PromiseFuture < ConfirmationInput, Void > promiseFuture = new PromiseFuture <>();

    public InformationSession(@NonNull String title,
                              @NonNull String text,
                              @NonNull String continueText,
                              @Nullable PolicyAction actionContext,
                              @Nullable Map<String, Object> clientContext){

        ViewGroup rootView = UIHandlerClientContext.getRootView(clientContext);
        AppCompatActivity currentActivity = (AppCompatActivity)(rootView.getContext());

        Fragment fragment = InformationFragment.newInstance(this, title, text, continueText);

        currentActivity.getSupportFragmentManager().beginTransaction()
                .add(rootView.getId(), fragment)
                .commit();
    }

    @Override
    public void onPromiseFutureComplete() {
        if(!promiseFuture.isDone()) {
            promiseFuture.complete(ConfirmationInput.create(-1));
        }
    }
}
