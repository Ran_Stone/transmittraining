package com.ts.training.authenticators.password;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.android.material.textfield.TextInputLayout;
import com.ts.training.R;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link PasswordFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class PasswordFragment extends Fragment {

    public interface PasswordFragmentInterface {
        public void onPromiseFutureComplete(String str);
        public void onPromiseFutureCancel();
    }

    private PasswordFragmentInterface listener;
    private EditText passwordEditText;
    private TextInputLayout passwordTextInputLayout;
    private TextView errorTextView;
    private Button continueBtn;
    private Button cancelBtn;

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public PasswordFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if(context instanceof PasswordFragmentInterface){
            this.listener = (PasswordFragmentInterface) context;
        } else {
            throw new RuntimeException("listener is not an instance of PasswordFragmentInterface");
        }
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment PasswordSessionFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static PasswordFragment newInstance(String param1, String param2) {
        PasswordFragment fragment = new PasswordFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.password_fragment, container, false);
        passwordTextInputLayout = view.findViewById(R.id.password_input);
        passwordEditText = view.findViewById(R.id.password_input_text);
//        errorTextView = view.findViewById(R.id.pwd_example_error);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        continueBtn = view.findViewById(R.id.password_btn_ok);
        cancelBtn = view.findViewById(R.id.password_btn_cancel);

        continueBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                continueBtn.setEnabled(false);
                cancelBtn.setEnabled(false);
                listener.onPromiseFutureComplete(passwordEditText.getText().toString());
            }
        });

        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                continueBtn.setEnabled(false);
                cancelBtn.setEnabled(false);
                listener.onPromiseFutureCancel();
            }
        });

    }

    public void showError(String error){

        passwordTextInputLayout.setErrorEnabled(true);
        passwordTextInputLayout.setError(error);

//        errorTextView.setText(error);
        continueBtn.setEnabled(true);
        cancelBtn.setEnabled(true);
//        passwordEditText.setText("");

    }
}