package com.ts.training.authenticators.password;

import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.ts.mobile.sdk.AuthenticationError;
import com.ts.mobile.sdk.AuthenticationErrorRecovery;
import com.ts.mobile.sdk.AuthenticatorDescription;
import com.ts.mobile.sdk.AuthenticatorSessionMode;
import com.ts.mobile.sdk.ControlRequest;
import com.ts.mobile.sdk.ControlRequestType;
import com.ts.mobile.sdk.InputOrControlResponse;
import com.ts.mobile.sdk.PasswordInput;
import com.ts.mobile.sdk.PolicyAction;
import com.ts.mobile.sdk.UIAuthenticatorSession;
import com.ts.mobile.sdk.util.PromiseFuture;
import com.ts.training.R;
import com.ts.training.authenticators.uiHandler.UIHandlerClientContext;

import java.util.List;
import java.util.Map;

/**
 * Created by Ran Stone on 01/11/2020.
 */
public class PasswordSession
        implements UIAuthenticatorSession<PasswordInput>, PasswordFragment.PasswordFragmentInterface {

    // this is the promise we will use to notify the sdk with either a password response or a cancel response
    private PromiseFuture<InputOrControlResponse<PasswordInput>, Void> promiseFuture;

    private AuthenticatorDescription description;
    private AuthenticatorSessionMode mode;
    private PolicyAction actionContext;
    private Map<String, Object> clientContext;

    private PasswordFragment fragment;
    private Boolean isRegistrationAfterExpiration = false;

    private String title;
    private String username;

    public PasswordSession(@NonNull String title, @NonNull String username){
        this.title = title;
        this.username = username;
    }

    /**
     * Get session parameters and initial status
     */
    @Override
    public void startSession(
            @NonNull AuthenticatorDescription description,
            @NonNull AuthenticatorSessionMode mode,
            @Nullable PolicyAction actionContext,
            @Nullable Map<String, Object> clientContext) {
        this.description = description;
        this.mode = mode;
        this.actionContext = actionContext;
        this.clientContext = clientContext;
    }

    /**
     * Once successful authentication is complete, If the authenticator is expired,
     * and the SDK now needs to register a new secret for it, it invokes the session
     * method UIAuthenticatorSession.changeSessionModeToRegistrationAfterExpiration.
     */
    @Override
    public void changeSessionModeToRegistrationAfterExpiration() {
        // TODO - not implemented yet
    }

    @Override
    public PromiseFuture<AuthenticationErrorRecovery, Void> promiseRecoveryForError(
            @NonNull AuthenticationError error,
            @NonNull List<AuthenticationErrorRecovery> validRecoveries,
            @NonNull AuthenticationErrorRecovery defaultRecovery) {

        PromiseFuture<AuthenticationErrorRecovery, Void> errorRecoveryVoidPromiseFuture = new PromiseFuture<>();

        if (defaultRecovery == AuthenticationErrorRecovery.RetryAuthenticator) {
            fragment.showError("Wrong input please try again");
            errorRecoveryVoidPromiseFuture.complete(AuthenticationErrorRecovery.RetryAuthenticator);

        } else if (defaultRecovery == AuthenticationErrorRecovery.Fail) {
//            Snackbar.make(rootView, "Fail to complete the policy", Snackbar.LENGTH_LONG).show();
            errorRecoveryVoidPromiseFuture.complete(defaultRecovery);

        }
//        else {
//            ErrorRecoveryMenu(currentActivity, validRecoveries, object :
//            ErrorRecoveryMenu.OnErrorRecoverySelectionListener {
//                override fun onErrorRecoverySelected(errorRecovery: AuthenticationErrorRecovery) {
//                    errorRecoveryPromiseFuture!!.let{
//                        it.complete(errorRecovery)
//                    }
//                }
//            })
//        }
        return errorRecoveryVoidPromiseFuture;
    }

    @Override
    public PromiseFuture<InputOrControlResponse<PasswordInput>, Void> promiseInput() {
        promiseFuture = new PromiseFuture<InputOrControlResponse<PasswordInput>, Void>();
        updateFragment();
        return promiseFuture;
    }

    private void updateFragment() {
        if(fragment == null){
            ViewGroup rootView = UIHandlerClientContext.getRootView(clientContext);
            AppCompatActivity currentActivity = (AppCompatActivity)(rootView.getContext());

            PasswordFragment.newInstance(
                currentActivity.getString(R.string.password_registration_title), username);

            currentActivity.getSupportFragmentManager().beginTransaction()
                    .add((rootView).getId(), fragment)
                    .commit();
        }
    }

    @Override
    public void endSession() {

    }

    @Override
    public void onPromiseFutureComplete(String str) {
        InputOrControlResponse<PasswordInput> response =
                InputOrControlResponse.createInputResponse(PasswordInput.create(str));
        // promiseFuture can only be consumed once. After consumed promiseInput() is called
        // and a new promiseFuture is generated
        if(!promiseFuture.isDone()) {
            promiseFuture.complete(response);
        }
    }

    @Override
    public void onPromiseFutureCancel() {
        // promiseFuture can only be consumed once. After consumed promiseInput() is called
        // and a new promiseFuture is generated
        if(!promiseFuture.isDone()) {
            promiseFuture.complete(InputOrControlResponse.<PasswordInput>createControlResponse(
                    ControlRequest.create(ControlRequestType.CancelAuthenticator)));
        }
    }
}
