package com.ts.training.authenticators.uiHandler;

import android.app.Activity;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.ts.common.internal.core.logger.Log;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by Ran Stone on 01/11/2020.
 */
public class UIHandlerClientContext {
    private static final String TAG = UIHandlerClientContext.class.getCanonicalName();
    private static final String ARG_ACTIVITY = "running_activity";
    private static final String ARG_VIEW = "running_parent_view";
    @Nullable
    private Map<String, Object> mLastClientContext;

    private static Activity getRunningActivity() {
        try {
            Class activityThreadClass = Class.forName("android.app.ActivityThread");
            Object activityThread = activityThreadClass.getMethod("currentActivityThread").invoke((Object)null);
            Field activitiesField = activityThreadClass.getDeclaredField("mActivities");
            activitiesField.setAccessible(true);
            Map activities = (Map)activitiesField.get(activityThread);
            Iterator var4 = activities.values().iterator();

            while(var4.hasNext()) {
                Object activityRecord = var4.next();
                Class activityRecordClass = activityRecord.getClass();
                Field pausedField = activityRecordClass.getDeclaredField("paused");
                pausedField.setAccessible(true);
                if (!pausedField.getBoolean(activityRecord)) {
                    Field activityField = activityRecordClass.getDeclaredField("activity");
                    activityField.setAccessible(true);
                    return (Activity)activityField.get(activityRecord);
                }
            }
        } catch (Exception var9) {
            throw new UIHandlerClientContext.ActivityNotFoundException(var9);
        }

        throw new UIHandlerClientContext.ActivityNotFoundException("Didn't find the running activity");
    }

    public UIHandlerClientContext() {
    }

    @NonNull
    Map<String, Object> createClientContextForPresentingActivity(Activity activity) {
        Map<String, Object> map = new HashMap();
        map.put(ARG_ACTIVITY, activity);
        this.mLastClientContext = map;
        return map;
    }

    @NonNull
    public Map<String, Object> createClientContextForPresentingView(ViewGroup parentView) {
        Map<String, Object> map = new HashMap();
        map.put(ARG_VIEW, parentView);
        this.mLastClientContext = map;
        return map;
    }

    @Nullable
    public ViewGroup getRootView() {
        return this.getRootView(this.mLastClientContext);
    }

    @Nullable
    public static ViewGroup getRootView(@Nullable Map<String, Object> clientContext) {
        ViewGroup view = null;
        if (clientContext != null) {
            if (clientContext.containsKey(ARG_ACTIVITY)) {
                view = getViewFromActivity((Activity)clientContext.get(ARG_ACTIVITY));
            } else if (clientContext.containsKey(ARG_VIEW)) {
                view = (ViewGroup)clientContext.get(ARG_VIEW);
            }
        }

        if (view != null) {
            return view;
        } else {
            Log.e(TAG, "No client context with views was found");
            return getViewFromActivity(getRunningActivity());
        }
    }

    private static ViewGroup getViewFromActivity(Activity activity) {
        return (ViewGroup)activity.findViewById(16908290);
    }

    public static class ActivityNotFoundException extends RuntimeException {
        ActivityNotFoundException(String message) {
            super(message);
        }

        ActivityNotFoundException(Throwable cause) {
            super(cause);
        }
    }

}
