package com.ts.training.authenticators.uiHandler;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.util.Log;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.ts.mobile.sdk.AudioAcquisitionStepDescription;
import com.ts.mobile.sdk.AudioInputResponse;
import com.ts.mobile.sdk.AuthenticationError;
import com.ts.mobile.sdk.AuthenticationOption;
import com.ts.mobile.sdk.AuthenticatorDescription;
import com.ts.mobile.sdk.AuthenticatorFallbackAction;
import com.ts.mobile.sdk.AuthenticatorSelectionResult;
import com.ts.mobile.sdk.CameraAcquisitionStepDescription;
import com.ts.mobile.sdk.CameraInputResponse;
import com.ts.mobile.sdk.ConfirmationInput;
import com.ts.mobile.sdk.ControlRequest;
import com.ts.mobile.sdk.ControlRequestType;
import com.ts.mobile.sdk.DeviceBiometricsInput;
import com.ts.mobile.sdk.FingerprintInput;
import com.ts.mobile.sdk.FingerprintPromptController;
import com.ts.mobile.sdk.InputResponseType;
import com.ts.mobile.sdk.JsonDataProcessingResult;
import com.ts.mobile.sdk.NativeFaceInput;
import com.ts.mobile.sdk.OtpTarget;
import com.ts.mobile.sdk.PasswordInput;
import com.ts.mobile.sdk.PatternInput;
import com.ts.mobile.sdk.PinInput;
import com.ts.mobile.sdk.PlaceholderInputResponse;
import com.ts.mobile.sdk.PolicyAction;
import com.ts.mobile.sdk.RedirectInput;
import com.ts.mobile.sdk.RedirectType;
import com.ts.mobile.sdk.ScanQrSession;
import com.ts.mobile.sdk.SecurityQuestionInputResponse;
import com.ts.mobile.sdk.SecurityQuestionStepDescription;
import com.ts.mobile.sdk.StartActivityReason;
import com.ts.mobile.sdk.UIApprovalsSession;
import com.ts.mobile.sdk.UIAuthenticationConfigurationSession;
import com.ts.mobile.sdk.UIAuthenticatorSession;
import com.ts.mobile.sdk.UIAuthenticatorSessionMobileApprove;
import com.ts.mobile.sdk.UIAuthenticatorSessionOtp;
import com.ts.mobile.sdk.UIAuthenticatorSessionTotp;
import com.ts.mobile.sdk.UIDeviceManagementSession;
import com.ts.mobile.sdk.UIFormSession;
import com.ts.mobile.sdk.UIHandler;
import com.ts.mobile.sdk.UIMultiInputAuthenticationSession;
import com.ts.mobile.sdk.UIPromotionSession;
import com.ts.mobile.sdk.UITicketWaitSession;
import com.ts.mobile.sdk.UITotpGenerationSession;
import com.ts.mobile.sdk.UnregistrationInput;
import com.ts.mobile.sdk.util.PromiseFuture;
import com.ts.mobile.sdk.util.defaults.DefaultUIHandler;
import com.ts.training.authenticators.information.InformationSession;
import com.ts.training.authenticators.password.PasswordSession;

import org.json.JSONObject;

import java.util.List;
import java.util.Map;

/**
 * Created by Ran Stone on 01/11/2020.
 */
public class UIHandlerManager implements UIHandler {

    private static final String TAG = UIHandlerManager.class.getCanonicalName();

    private static UIHandlerManager instance;
    private final UIHandlerClientContext uiHandlerClientContext = new UIHandlerClientContext();

    private ProgressDialog mProgressDialog;

    public static UIHandlerManager getInstance() {
        if (instance == null) {
            instance = new UIHandlerManager();
        }
        return instance;
    }

    @NonNull
    public static Map<String, Object> createClientContextForPresentingActivity(Activity activity) {
        return getInstance().uiHandlerClientContext.createClientContextForPresentingActivity(activity);
    }

    @NonNull
    public static Map<String, Object> createClientContextForPresentingView(ViewGroup parentView) {
        return getInstance().uiHandlerClientContext.createClientContextForPresentingView(parentView);
    }

    @Override
    public void startActivityIndicator(@Nullable PolicyAction actionContext, @Nullable Map<String, Object> clientContext) {
        if (this.mProgressDialog == null || !this.mProgressDialog.isShowing()) {
            try {
                this.mProgressDialog = ProgressDialog.show(this.uiHandlerClientContext.getRootView(clientContext).getContext(), (CharSequence)null, "Authenticating...", false, false);
            } catch (Throwable var4) {
                Log.e(TAG, "Failed to startActivityIndicator()", var4);
            }
        }
    }

    @Override
    public void endActivityIndicator(@Nullable PolicyAction actionContext, @Nullable Map<String, Object> clientContext) {
        if (this.mProgressDialog != null && this.mProgressDialog.isShowing()) {
            this.mProgressDialog.dismiss();
        }
    }

    @Override
    public void controlFlowCancelled(@Nullable Map<String, Object> clientContext) {

    }

    @Override
    public void controlFlowStarting(@Nullable Map<String, Object> clientContext) {

    }

    @Override
    public void controlFlowEnded(@Nullable AuthenticationError error, @Nullable Map<String, Object> clientContext) {

    }

    @Override
    public void controlFlowActionStarting(@Nullable PolicyAction actionContext, @Nullable Map<String, Object> clientContext) {

    }

    @Override
    public void controlFlowActionEnded(@Nullable AuthenticationError error, @Nullable PolicyAction actionContext, @Nullable Map<String, Object> clientContext) {

    }

    @Override
    public PromiseFuture<UnregistrationInput, Void> handleAuthenticatorUnregistration(@NonNull AuthenticatorDescription authenticatorDescription, @NonNull Boolean isPlaceholder, @Nullable PolicyAction actionContext, @Nullable Map<String, Object> clientContext) {
        return null;
    }

    @Override
    public Boolean shouldIncludeDisabledAuthenticatorsInMenu(@Nullable PolicyAction actionContext, @Nullable Map<String, Object> clientContext) {
        return null;
    }

    @Override
    public PromiseFuture<AuthenticatorSelectionResult, Void> selectAuthenticator(@NonNull List<AuthenticationOption> options, @Nullable PolicyAction actionContext, @Nullable Map<String, Object> clientContext) {
        return null;
    }

    @Override
    public PromiseFuture<AuthenticatorFallbackAction, Void> selectAuthenticatorFallbackAction(@NonNull List<AuthenticatorFallbackAction> validOptions, @Nullable AuthenticatorDescription fallbackAuth, @NonNull UIAuthenticatorSession<InputResponseType> session, @Nullable PolicyAction actionContext, @Nullable Map<String, Object> clientContext) {
        return null;
    }

    @Override
    public PromiseFuture<ControlRequest, Void> controlOptionForCancellationRequestInSession(@NonNull List<ControlRequestType> validOptions, @NonNull UIAuthenticatorSession<InputResponseType> session) {
        return null;
    }

    @Override
    public UIAuthenticatorSession<PasswordInput> createPasswordAuthSession(@NonNull String title, @NonNull String username) {
        return new PasswordSession(title, username);
    }

    @Override
    public UIAuthenticatorSession<FingerprintInput> createFingerprintAuthSession(@NonNull String title, @NonNull String username) {
        return null;
    }

    @Override
    public UIAuthenticatorSession<NativeFaceInput> createNativeFaceAuthSession(@NonNull String title, @NonNull String username) {
        return null;
    }

    @Override
    public UIAuthenticatorSession<DeviceBiometricsInput> createDeviceBiometricsAuthSession(@NonNull String title, @NonNull String username) {
        return null;
    }

    @Override
    public UIAuthenticatorSession<PinInput> createPinAuthSession(@NonNull String title, @NonNull String username, @NonNull Integer pinLength) {
        return null;
    }

    @Override
    public UIAuthenticatorSession<PatternInput> createPatternAuthSession(@NonNull String title, @NonNull String username, @NonNull Integer gridWidth, @NonNull Integer gridHeight) {
        return null;
    }

    @Override
    public UIAuthenticatorSessionOtp createOtpAuthSession(@NonNull String title, @NonNull String username, @NonNull List<OtpTarget> possibleTargets, @Nullable OtpTarget autoExecedTarget) {
        return null;
    }

    @Override
    public UIMultiInputAuthenticationSession<CameraInputResponse, CameraAcquisitionStepDescription> createFaceAuthSession(@NonNull String title, @NonNull String username) {
        return null;
    }

    @Override
    public UIMultiInputAuthenticationSession<AudioInputResponse, AudioAcquisitionStepDescription> createVoiceAuthSession(@NonNull String title, @NonNull String username) {
        return null;
    }

    @Override
    public UIAuthenticatorSession<PlaceholderInputResponse> createPlaceholderAuthSession(@NonNull String placeholderName, @NonNull String placeholderType, @NonNull String title, @NonNull String username, @NonNull String authenticatorConfiguredData, @NonNull String serverPayload) {
        return null;
    }

    @Override
    public UIAuthenticatorSessionMobileApprove createMobileApproveAuthSession(@NonNull String title, @NonNull String username, @NonNull String instructions) {
        return null;
    }

    @Override
    public UIAuthenticatorSessionTotp createTotpAuthSession(@NonNull String title, @NonNull String username) {
        return null;
    }

    @Override
    public UIMultiInputAuthenticationSession<SecurityQuestionInputResponse, SecurityQuestionStepDescription> createSecurityQuestionAuthSession(@NonNull String title, @NonNull String username) {
        return null;
    }

    @Override
    public PromiseFuture<ConfirmationInput, Void> getConfirmationInput(@NonNull String title, @NonNull String text, @NonNull String continueText, @NonNull String cancelText, @Nullable PolicyAction actionContext, @Nullable Map<String, Object> clientContext) {
        return null;
    }

    @Override
    public PromiseFuture<ConfirmationInput, Void> getInformationResponse(
            @NonNull String title,
            @NonNull String text,
            @NonNull String continueText,
            @Nullable PolicyAction actionContext,
            @Nullable Map<String, Object> clientContext) {
        return new InformationSession(title, text, continueText, actionContext, clientContext);
    }

    @Override
    public ScanQrSession createScanQrSession(@Nullable PolicyAction actionContext, @Nullable Map<String, Object> clientContext) {
        return null;
    }

    @Override
    public UITicketWaitSession createTicketWaitSession(@Nullable PolicyAction actionContext, @Nullable Map<String, Object> clientContext) {
        return null;
    }

    @Override
    public UIFormSession createFormSession(@NonNull String formId, @NonNull JSONObject payload) {
        return null;
    }

    @Override
    public UIAuthenticationConfigurationSession createAuthenticationConfigurationSession(@NonNull String userId) {
        return null;
    }

    @Override
    public UIPromotionSession createRegistrationPromotionSession(@NonNull String userId, @Nullable PolicyAction actionContext) {
        return null;
    }

    @Override
    public UIDeviceManagementSession createDeviceManagementSession(@NonNull String userId) {
        return null;
    }

    @Override
    public UIApprovalsSession createApprovalsSession(@NonNull String userId) {
        return null;
    }

    @Override
    public UITotpGenerationSession createTotpGenerationSession(@NonNull String userId, @Nullable String generatorName) {
        return null;
    }

    @Override
    public PromiseFuture<JsonDataProcessingResult, Void> processJsonData(@NonNull JSONObject jsonData, @Nullable PolicyAction actionContext, @Nullable Map<String, Object> clientContext) {
        return null;
    }

    @Override
    public PromiseFuture<ConfirmationInput, Void> handlePolicyRejection(@Nullable String title, @Nullable String text, @Nullable String buttonText, @Nullable JSONObject failureData, @Nullable PolicyAction actionContext, @Nullable Map<String, Object> clientContext) {
        return null;
    }

    @Override
    public PromiseFuture<RedirectInput, Void> handlePolicyRedirect(@NonNull RedirectType redirectType, @Nullable String policyId, @Nullable String userId, @Nullable JSONObject additionalParameters, @Nullable Map<String, Object> clientContext) {
        return null;
    }

    @Override
    public FingerprintPromptController createFingerprintPromptController(@NonNull UIAuthenticatorSession<FingerprintInput> session) {
        return null;
    }

    @Override
    public void startActivity(@NonNull Intent intent, @NonNull StartActivityReason reason, @Nullable PolicyAction actionContext, @Nullable Map<String, Object> clientContext) {

    }

    @Override
    public PromiseFuture<Boolean, Void> localAuthenticatorInvalidated(@NonNull AuthenticatorDescription description, @Nullable Map<String, Object> clientContext) {
        return null;
    }
}
