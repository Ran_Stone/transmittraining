package com.ts.training.ui.login;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.android.material.snackbar.Snackbar;
import com.ts.mobile.sdk.AuthenticationError;
import com.ts.mobile.sdk.AuthenticationResult;
import com.ts.mobile.sdk.TransmitSDK;
import com.ts.mobile.sdk.util.ObservableFuture;
import com.ts.mobile.sdk.util.defaults.DefaultUIHandler;
import com.ts.training.R;
import com.ts.training.authenticators.uiHandler.UIHandlerClientContext;
import com.ts.training.authenticators.uiHandler.UIHandlerManager;
import com.ts.training.ui.main.MainActivity;

import org.json.JSONObject;

import java.util.Map;

public class LoginFragment extends Fragment {

    public static LoginFragment newInstance() {
        return new LoginFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.login_fragment, container, false);
        setSubmitButtonAction(view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    public void setSubmitButtonAction(final View view) {
        final Button button = view.findViewById(R.id.bind_or_login_button);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                final EditText editText = view.findViewById(R.id.username_input_text);
                final String userName = editText.getText().toString();
                if (userName.length() == 0) {
                    Snackbar.make(v, "Please enter your username to continue!", Snackbar.LENGTH_LONG).show();
                    return;
                }
                loginOrBindUser(userName, view);
            }
        });
    }

    private void loginOrBindUser(String username, View view) {
        Map<String, Object> clientContext = UIHandlerManager.getInstance().createClientContextForPresentingView((ViewGroup)view.getParent());
        final JSONObject params = new JSONObject();

        if (!TransmitSDK.getInstance().isBoundForUser(username)) {
            TransmitSDK.getInstance().bind(username, params, clientContext)
                    .addListener(new ObservableFuture.Listener<AuthenticationResult, AuthenticationError>() {
                        @Override
                        public void onComplete(AuthenticationResult authenticationResult) {
//                            showToast("Bind Success!");
                            showDemo();
                        }

                        @Override
                        public void onReject(AuthenticationError authenticationError) {
                            showToast("Bind failed!");
                        }
                    });
        } else {
            TransmitSDK.getInstance().authenticate(username, "Login", params, clientContext)
                    .addListener(new ObservableFuture.Listener<AuthenticationResult, AuthenticationError>() {
                        @Override
                        public void onComplete(AuthenticationResult authenticationResult) {
//                            showToast("Authenticated Success!!");
                            showDemo();
                        }

                        @Override
                        public void onReject(AuthenticationError authenticationError) {
                            Log.d("Auth Error!", authenticationError.getMessage());
                            showToast("Authenticated failed!!!");
                        }
                    });
        }

    }

    private void showDemo() {
        Intent myIntent = new Intent(getActivity(), MainActivity.class);
        getActivity().startActivity(myIntent);
    }

    private void showToast(CharSequence message) {
        Context context = getActivity();
        int duration = Toast.LENGTH_SHORT;
        Toast toast = Toast.makeText(context, message, duration);
        toast.setGravity(Gravity.TOP| Gravity.CENTER_VERTICAL, 0, 120);
        toast.show();
    }


}