package com.ts.training.ui.main;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.google.android.material.snackbar.Snackbar;
import com.ts.mobile.sdk.AuthenticationError;
import com.ts.mobile.sdk.AuthenticationResult;
import com.ts.mobile.sdk.TransmitSDK;
import com.ts.mobile.sdk.util.ObservableFuture;
import com.ts.mobile.sdk.util.PromiseFuture;
import com.ts.mobile.sdk.util.defaults.DefaultUIHandler;
import com.ts.training.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;

public class MainActivity extends AppCompatActivity {

    final static String TAG = MainActivity.class.getSimpleName();

    View container;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);

//        container = findViewById(R.id.demo_activity_container);

        setInvokePolicyAction();
        setConfigurationAction();
        setCustomUIFlowButton();
        setLogoutAction();
    }

    /**
     * TransmitSDKXm.invokePolicy.
     * Invoking this call requests the Transmit Server to run a policy within the current primary device session.
     * This method is very similar in operation to the authenticate method with the main difference being that it does not
     * take a user handle as a parameter since it uses the user handle of the current primary device session.
     */
    private void setInvokePolicyAction() {
        final String POLICY_ID = "money_transfer";
//        final Map<String, Object> clientContext = DefaultUIHandler.createClientContextForPresentingActivity(this);
//        final JSONObject params = new JSONObject();
//        try {
//            params.put("amount", 2000);
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }

//        Button button = findViewById(R.id.invoke_policy_button);
//        String policyTitle = button.getText().toString();
//        policyTitle += ": " + POLICY_ID;
//        button.setText(policyTitle);
//
//        button.setOnClickListener(new View.OnClickListener() {
//            public void onClick(final View v) {
//                openAmountDialog();
//            }
//        });
    }

    private void openAmountDialog() {

        // Get the layout inflater
        LayoutInflater inflater = getLayoutInflater();
//        View dialogView = inflater.inflate(R.layout.money_transfer_amount_dialog, null);
//        final EditText amountET = dialogView.findViewById(R.id.money_transfer_amount);
//
//        new AlertDialog.Builder(this)
//                .setView(dialogView)
//                .setPositiveButton(R.string.submit, new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//                        dialog.dismiss();
//                        invokeMoneyTransferPolicy(amountET.getText().toString());
//                    }
//                })
//                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//                        dialog.dismiss();
//                    }
//                })
//                .setCancelable(false)
//                .show();
    }

    private void invokeMoneyTransferPolicy(String amount) {
        final String POLICY_ID = "money_transfer";
        final Map<String, Object> clientContext = DefaultUIHandler.createClientContextForPresentingActivity(this);
        final JSONObject params = new JSONObject();
        try {
            params.put("amount", Integer.parseInt(amount));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        // we can invoke any journey that's defined in our Transmit Console
        PromiseFuture promise = TransmitSDK.getInstance().invokePolicy(POLICY_ID, params, clientContext);
        promise.addListener(new ObservableFuture.Listener() {
            @Override
            public void onComplete(Object o) {
                AuthenticationResult result = (AuthenticationResult)o;
                Log.d("invokePolicy"," Data:" + result.getData().toString());
                Snackbar.make(container, "You have transferred $2000 ", Snackbar.LENGTH_LONG).show();
            }

            @Override
            public void onReject(Object o) {
                Log.e(TAG, ((AuthenticationError)o).getMessage());
//                Snackbar snackbar = Snackbar.make(container, "Money transfer ended with an error ", Snackbar.LENGTH_LONG);
//                snackbar.getView().setBackgroundColor(ContextCompat.getColor(DemoActivity.this, R.color.text_error));
            }
        });

    }

    private void setConfigurationAction() {
//        final Map<String, Object> clientContext = DefaultUIHandler.createClientContextForPresentingActivity(this);
//        final JSONObject params = new JSONObject();
//
//        Button button = findViewById(R.id.configuration_button);
//        button.setOnClickListener(new View.OnClickListener() {
//            public void onClick(View v) {
//                PromiseFuture promise = TransmitSDK.getInstance().startAuthenticationConfiguration(clientContext);
//                promise.addListener(new ObservableFuture.Listener() {
//                    @Override
//                    public void onComplete(Object o) {
//                        showToast("Device Configurations Journey Completed");
//                    }
//
//                    @Override
//                    public void onReject(Object o) {
//                        showToast("JDevice Configurations Journey ended with error!");
//                    }
//                });
//            }
//        });

        // 2. Run the app and try the Configuration Action
    }

    private void setLogoutAction() {
//        Button button = findViewById(R.id.logout_button);
//        button.setOnClickListener(new View.OnClickListener() {
//            public void onClick(View v) {
//                PromiseFuture promise = TransmitSDK.getInstance().logout();
//                promise.addListener(new ObservableFuture.Listener() {
//                    @Override
//                    public void onComplete(Object o) {
////                        onBackPressed();
//                        Intent myIntent = new Intent(DemoActivity.this, MainActivity.class);
//                        DemoActivity.this.startActivity(myIntent);
//
//                    }
//
//                    @Override
//                    public void onReject(Object o) {
//                        showToast("Error logging out...");
//                    }
//                });
//            }
//        });
    }

    private void setCustomUIFlowButton() {
//        TransmitSDK.getInstance().setUiHandler(new MyUIHandler());

//        final String customUIPolicy = "example_journey_1";
//        final Map<String, Object> clientContext = DefaultUIHandler.createClientContextForPresentingActivity(this);
//        final JSONObject params = new JSONObject();
//
//        Button button = findViewById(R.id.custom_ui_button);
//        button.setOnClickListener(new View.OnClickListener() {
//            public void onClick(View v) {
//                PromiseFuture promise = TransmitSDK.getInstance().invokePolicy(customUIPolicy, params, clientContext);
//                promise.addListener(new ObservableFuture.Listener() {
//                    @Override
//                    public void onComplete(Object o) {
//                        showToast("Journey Completed");
//                    }
//
//                    @Override
//                    public void onReject(Object o) {
//                        showToast("Journey ended with error!");
//                    }
//                });
//            }
//        });
    }


    private void showToast(CharSequence message) {
        Context context = getApplicationContext();
        int duration = Toast.LENGTH_SHORT;
        Toast toast = Toast.makeText(context, message, duration);
        toast.setGravity(Gravity.TOP| Gravity.CENTER_VERTICAL, 0, 120);
        toast.show();
    }

    private void dismissActivity() {
        onBackPressed();
    }
}
